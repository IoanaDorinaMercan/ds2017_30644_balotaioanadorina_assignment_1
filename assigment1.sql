CREATE DATABASE  IF NOT EXISTS `assignment1`;
USE `assignment1`;

DROP TABLE IF EXISTS `city`;
CREATE TABLE `city` (
  `cityId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  PRIMARY KEY (`cityId`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `flight`;
CREATE TABLE `flight` (
  `flightId` int(11) NOT NULL AUTO_INCREMENT,
  `flightNumber` int(11) DEFAULT NULL,
  `airplaneType` varchar(255) DEFAULT NULL,
  `departureCity` int(11) DEFAULT NULL,
  `departureDateAndHour` datetime DEFAULT NULL,
  `arrivalCity` int(11) DEFAULT NULL,
  `arrivalDateAndHour` datetime DEFAULT NULL,
  PRIMARY KEY (`flightId`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `isAdmin` bit(1) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
