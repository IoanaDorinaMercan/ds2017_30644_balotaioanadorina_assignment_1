package main.resources.business;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import main.resources.data.CityDAO;
import main.resources.data.FlightDAO;
import main.resources.entities.City;
import main.resources.entities.Flight;

/**
 * Servlet implementation class AdministratorServlet
 */
public class AdministratorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		if (request.getSession().getAttribute("isAdmin") == null) {
			out.println("<html>");
			out.println("<head>");
			out.println("<title>Error login</title>");
			out.println("</head>");
			out.println("<body>");
			out.println("Can't access admin page!<br>");
			out.println("</body>");
			out.println("</html>");
			out.close();
		} else {
			String role = request.getSession().getAttribute("isAdmin").toString();
			if (role.equals("true")) {
				out.println("<html>\n" + "<head><title>Admin Options</title></head>" + "<body>"
						+ "<h1>Administrator Options</h1>" + "<h2>Choose one of following options</h2>" + "<br></br>" +

						"<form method=post action=AdministratorServlet>" + "Airplane type	"
						+ "<input type=text name=airplaneType>" + "<br>" + "Departure city name	 "
						+ "<input type=text name=departureCityName>" + "<br>" + "Departure city latitude	 "
						+ "<input type=text name=departureCityLatitude>" + "<br>" + "Departure city longitude	 "
						+ "<input type=text name=departureCityLongitude>" + "<br>" + "Departure city date&hour	 "
						+ "<input type=datetime-local value=\"2017-06-01 08:30\" name=departureCityDateAndHour>"
						+ "<br>" + "Arrival city name	 " + "<input type=text name=arrivalCityName>" + "<br>"
						+ "Arrival city latitude	 " + "<input type=text name=arrivalCityLatitude>" + "<br>"
						+ "Arrival city longitude	 " + "<input type=text name=arrivalCityLongitude>" + "<br>"
						+ "Arrival city date&hour	 "
						+ "<input type=datetime-local value=\"2017-06-01 08:30\" name=arrivalCityDateAndHour>" + "<br>"
						+ "Flight number	" + "<input type=text name=flightNumber>" + "<br>"
						+ "<button type=submit name=option value=add>Add Flight</button>"
						+ "<button type=submit name=option value=delete>Delete Flight</button>"
						+ "<button type=submit name=option value=update>Update Flight</button>"
						+ "<button type=submit name=option value=get>View informations about all flights</button>"
						+ "<button type=submit name=option value=logout>Logout</button>" + "</form>" + "</html>"
						+ "</body>");

				out.close();
			} else {
				out.println("<html>");
				out.println("<head>");
				out.println("<title>Error login</title>");
				out.println("</head>");
				out.println("<body>");
				out.println("You are not allowed to access the admin page!<br>");
				out.println("</body>");
				out.println("</html>");
				out.close();
			}
		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		if (request.getSession().getAttribute("isAdmin").toString().equals("true")) {
			if (request.getParameter("option").toString().equals("logout")) {
				request.getSession().invalidate();
				response.sendRedirect("LoginServlet");
			} else {
				response.setContentType("text/html");
				PrintWriter out = response.getWriter();

				String departureDH = request.getParameter("departureCityDateAndHour");
				String arrivalDH = request.getParameter("arrivalCityDateAndHour");
				Date departureCityDateAndHour = null;
				Date arrivalCityDateAndHour = null;
				try {
					departureCityDateAndHour = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(departureDH);
					arrivalCityDateAndHour = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(arrivalDH);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				Timestamp timestampD = new Timestamp(departureCityDateAndHour.getTime());
				Timestamp timestampA = new Timestamp(arrivalCityDateAndHour.getTime());

				String arrivalLatitude = request.getParameter("arrivalCityLatitude");
				String arrivalLongitude = request.getParameter("arrivalCityLongitude");
				String departureLatitude = request.getParameter("departureCityLatitude");
				String departureLongitude = request.getParameter("departureCityLongitude");
				String flightNumber = request.getParameter("flightNumber");

				FlightDAO flightDAO = new FlightDAO();
				Flight flight = new Flight();
				flight.setAirplaneType(request.getParameter("airplaneType"));
				flight.setDepartureDateAndHour(timestampD);
				flight.setArrivalDateAndHour(timestampA);
				flight.setFlightNumber(Integer.valueOf(request.getParameter("flightNumber")));

				City departureCity = new City();
				departureCity.setLatitude(Double.valueOf(departureLatitude));
				departureCity.setLongitude(Double.valueOf(departureLongitude));
				departureCity.setName(request.getParameter("departureCityName"));

				City arrivalCity = new City();
				arrivalCity.setLatitude(Double.valueOf(arrivalLatitude));
				arrivalCity.setLongitude(Double.valueOf(arrivalLongitude));
				arrivalCity.setName(request.getParameter("arrivalCityName"));

				CityDAO cityDAO = new CityDAO();
				arrivalCity.setId(cityDAO.addCity(arrivalCity));
				departureCity.setId(cityDAO.addCity(departureCity));

				flight.setArrivalCity(arrivalCity.getId());
				flight.setDepartureCity(departureCity.getId());

				String option = request.getParameter("option");
				if (option.equals("add")) {
					int id = flightDAO.addFlight(flight);
					if (id > 0) {
						out.println("<html>");
						out.println("<head>");
						out.println("<title>Successfuly added</title>");
						out.println("</head>");
						out.println("<body>");
						out.println("Successfuly added<br>");
						out.println("</body>");
						out.println("</html>");

						out.close();
					} else {
						out.println("<html>");
						out.println("<head>");
						out.println("<title>Error adding a new flight</title>");
						out.println("</head>");
						out.println("<body>");
						out.println("Error adding a new flight<br>");
						out.println("</body>");
						out.println("</html>");
						out.close();
					}

				}
				if (option.equals("update")) {
					int id = flightDAO.updateFlight(Integer.valueOf(flightNumber), flight);
					if (id > 0) {
						out.println("<html>");
						out.println("<head>");
						out.println("<title>Successfuly updated</title>");
						out.println("</head>");
						out.println("<body>");
						out.println("Flight with number: " + flightNumber + " was succssfully updated<br>");
						out.println("</body>");
						out.println("</html>");

						out.close();
					} else {
						out.println("<html>");
						out.println("<head>");
						out.println("<title>Error updating flight</title>");
						out.println("</head>");
						out.println("<body>");
						out.println("Error updating flight!<br>");
						out.println("</body>");
						out.println("</html>");
						out.close();
					}
				}

				if (option.equals("delete")) {
					int id = flightDAO.deleteFlight(Integer.valueOf(flightNumber));
					if (id > 0) {
						out.println("<html>");
						out.println("<head>");
						out.println("<title>Successfuly deleted</title>");
						out.println("</head>");
						out.println("<body>");
						out.println("Flight with number: " + flightNumber + " was succssfully deleted<br>");
						out.println("</body>");
						out.println("</html>");

						out.close();
					} else {
						out.println("<html>");
						out.println("<head>");
						out.println("<title>Error deleting flight</title>");
						out.println("</head>");
						out.println("<body>");
						out.println("Error deleting flight!<br>");
						out.println("</body>");
						out.println("</html>");
						out.close();
					}

				}
				if (option.equals("get")) {
					List<Flight> flights = flightDAO.getAllFlights();
					System.out.println(flights.size());
					String outFlights = "";
					for (int i = 0; i < flights.size(); i++) {
						outFlights += "<tr>";
						outFlights += "<td>" + flights.get(i).getFlightId() + "</td>";
						outFlights += "<td>" + flights.get(i).getFlightNumber() + "</td>";
						outFlights += "<td>" + flights.get(i).getAirplaneType() + "</td>";
						outFlights += "<td>" + flights.get(i).getDepartureCity() + "</td>";
						outFlights += "<td>" + cityDAO.getCity(flights.get(i).getDepartureCity()).getName() + "</td>";
						outFlights += "<td>" + flights.get(i).getDepartureDateAndHour() + "</td>";
						outFlights += "<td>" + flights.get(i).getArrivalCity() + "</td>";
						outFlights += "<td>" + cityDAO.getCity(flights.get(i).getArrivalCity()).getName() + "</td>";
						outFlights += "<td>" + flights.get(i).getArrivalDateAndHour() + "</td>";
						outFlights += "</tr>";
					}

					out.println("<html>" + "<head>" + "<style>" + "table{width:100%;}" + "td,th{text-align:center;}"
							+ "</style>" + "</head>" + "<body>" + "<table>" + "<tr>" + "<th>Flight Id</th>"
							+ "<th>Flight Number</th>" + "<th>Airplane type</th>" + "<th>Departure City Id</th>"
							+ "<th>Departure city name</th>" + "<th>Departure city date&hour</th>"
							+ "<th>Arrival City Id</th>" + "<th>Arrival city name</th>"
							+ "<th>Arrival city date&hour</th>" + "</tr>" + outFlights + "</table>" + "</body>"
							+ "</html>");
					out.close();

				}

			}
		}
	}

}
