package main.resources.business;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import main.resources.data.UserDao;
import main.resources.entities.User;

/**
 * Servlet implementation class LoginServlet
 */
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserDao userDAO;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println(
				"<html>\n" + "<head><title>LoginPage</title></head>" + "<body>" + "<h1>Login Page</h1>" + "<br></br>"
						+ "<form method=post action=LoginServlet>" + "Username	" + "<input type=text name=username>"
						+ "<br>" + "Password	 " + "<input type=password name=password>" + "<br>" + "<br>" + "<br>"
						+ "<button type=submit value=submit>Login</button>" + "</form>" + "</html>" + "</body>");
		out.close();

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		userDAO = new UserDao();
		User user = userDAO.findUser(request.getParameter("username"), request.getParameter("password"));

		if (user == null) {
			out.println("<html>" + "<head>" + "<title>User with this username and password doesn't exists.</title>"
					+ "</head>" + "<body>" + "<center>" + "<h1>User with this username and password doesn't exist.</h1>"
					+ "<h2>Please go back and try another username and password!.</h2>"
					+ "<form method=get action=LoginServlet>" + "<button type=submit value=submit>Go Back</button>"
					+ "</form>" + "</center>" + "</body>" + "</html>");
			out.close();
		} else {
			if (user.getIsAdmin() == true) {
				request.getSession().setAttribute("username", request.getParameter("username"));
				request.getSession().setAttribute("password", request.getParameter("password"));
				request.getSession().setAttribute("isAdmin", user.getIsAdmin());
				response.sendRedirect("AdministratorServlet");

			} else {
				request.getSession().setAttribute("username", request.getParameter("username"));
				request.getSession().setAttribute("password", request.getParameter("password"));
				request.getSession().setAttribute("isAdmin", user.getIsAdmin());
				response.sendRedirect("UserServlet");
			}
		}

	}

}
