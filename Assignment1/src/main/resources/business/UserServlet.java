package main.resources.business;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import main.resources.data.CityDAO;
import main.resources.data.FlightDAO;
import main.resources.entities.City;
import main.resources.entities.Flight;

/**
 * Servlet implementation class UserServlet
 */
public class UserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		if (request.getSession().getAttribute("isAdmin") == null) {
			out.println("<html>");
			out.println("<head>");
			out.println("<title>Error login</title>");
			out.println("</head>");
			out.println("<body>");
			out.println("Can't access client page!<br>");
			out.println("</body>");
			out.println("</html>");
			out.close();
		} else {
			String role = request.getSession().getAttribute("isAdmin").toString();
			if (role.equals("false")) {
				FlightDAO flightDAO = new FlightDAO();
				List<Flight> flights = flightDAO.getAllFlights();

				CityDAO cityDAO = new CityDAO();
				String outFlights = "";
				for (int i = 0; i < flights.size(); i++) {
					outFlights += "<tr>";
					outFlights += "<td>" + flights.get(i).getFlightId() + "</td>";
					outFlights += "<td>" + flights.get(i).getFlightNumber() + "</td>";
					outFlights += "<td>" + flights.get(i).getAirplaneType() + "</td>";
					outFlights += "<td>" + flights.get(i).getDepartureCity() + "</td>";
					outFlights += "<td>" + cityDAO.getCity(flights.get(i).getDepartureCity()).getName() + "</td>";
					outFlights += "<td>" + flights.get(i).getDepartureDateAndHour() + "</td>";
					outFlights += "<td>" + flights.get(i).getArrivalCity() + "</td>";
					outFlights += "<td>" + cityDAO.getCity(flights.get(i).getArrivalCity()).getName() + "</td>";
					outFlights += "<td>" + flights.get(i).getArrivalDateAndHour() + "</td>";
					outFlights += "</tr>";
				}

				out.println("<html>" + "<head>" + "<style>" + "table{width:100%;}" + "td,th{text-align:center;}"
						+ "</style>" + "</head>" + "<body>" + "<table>" + "<tr>" + "<th>Flight Id</th>"
						+ "<th>Flight Number</th>" + "<th>Airplane type</th>" + "<th>Departure City Id</th>"
						+ "<th>Departure city name</th>" + "<th>Departure city date&hour</th>"
						+ "<th>Arrival City Id</th>" + "<th>Arrival city name</th>" + "<th>Arrival city date&hour</th>"
						+ "</tr>" + outFlights + "</table>" +

						"<form method=post action=UserServlet>" + "Enter flight number	"
						+ "<input type=text name=flightNumber>" + "<br>"
						+ "<button type=submit name=option value=get>View local time</button>"
						+ "<button type=submit name=option value=logout>Logout</button>" + "</form>" + "</body>"
						+ "</html>");
				out.close();
			} else {
				out.println("<html>");
				out.println("<head>");
				out.println("<title>Error login</title>");
				out.println("</head>");
				out.println("<body>");
				out.println("Can't access client page!<br>");
				out.println("</body>");
				out.println("</html>");
				out.close();
			}
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (request.getSession().getAttribute("isAdmin").toString().equals("false")) {
			if (request.getParameter("option").toString().equals("get")) {
				CityDAO cityDAO = new CityDAO();

				FlightDAO flightDAO = new FlightDAO();
				String flightNumber = request.getParameter("flightNumber");
				Flight flight = flightDAO.getFlight(Integer.valueOf(flightNumber));

				// departure city
				City city = cityDAO.getCity(flight.getDepartureCity());
				String uri = "http://www.new.earthtools.org/timezone/" + city.getLatitude() + "/" + city.getLongitude();
				URL url = new URL(uri);
				HttpURLConnection connection = (HttpURLConnection) url.openConnection();

				BufferedReader bufferedReader = new BufferedReader(
						new InputStreamReader((connection.getInputStream())));
				String output;
				String result = "";
				while ((output = bufferedReader.readLine()) != null) {

					result += output;
				}

				response.setContentType("text/html");
				PrintWriter out = response.getWriter();

				String s = null;
				s = result.split("<" + "localtime" + ">")[1].split("</" + "localtime" + ">")[0];
				out.println("Departure city local time: ");
				out.println(s);
				out.println("<br>");
				bufferedReader.close();
				connection.disconnect();

				// arrival city
				city = cityDAO.getCity(flight.getArrivalCity());
				uri = "http://www.new.earthtools.org/timezone/" + city.getLatitude() + "/" + city.getLongitude();
				url = new URL(uri);
				connection = (HttpURLConnection) url.openConnection();

				bufferedReader = new BufferedReader(new InputStreamReader((connection.getInputStream())));
				result = "";
				while ((output = bufferedReader.readLine()) != null) {

					result += output;
				}

				bufferedReader.close();
				connection.disconnect();

				out.println("Arrival city local time: ");
				s = result.split("<" + "localtime" + ">")[1].split("</" + "localtime" + ">")[0];
				out.println(s);
				out.close();
			}

			if (request.getParameter("option").toString().equals("logout")) {
				request.getSession().invalidate();
				response.sendRedirect("LoginServlet");
			}

		}
	}

}
