package main.resources.entities;

import java.util.Date;


public class Flight {

	private int flightId;
	
	private int flightNumber;
	
	private String airplaneType;
	
	private int departureCity;
	
	private Date departureDateAndHour;
	
	private int arrivalCity;
	
	private Date arrivalDateAndHour;

	public int getFlightId() {
		return flightId;
	}

	public void setFlightId(int flightId) {
		this.flightId = flightId;
	}

	public String getAirplaneType() {
		return airplaneType;
	}

	public void setAirplaneType(String airplaneType) {
		this.airplaneType = airplaneType;
	}

	public int getDepartureCity() {
		return departureCity;
	}

	public void setDepartureCity(int departureCity) {
		this.departureCity = departureCity;
	}

	public Date getDepartureDateAndHour() {
		return departureDateAndHour;
	}

	public void setDepartureDateAndHour(Date departureDateAndHour) {
		this.departureDateAndHour = departureDateAndHour;
	}

	public int getArrivalCity() {
		return arrivalCity;
	}

	public void setArrivalCity(int arrivalCity) {
		this.arrivalCity = arrivalCity;
	}

	public Date getArrivalDateAndHour() {
		return arrivalDateAndHour;
	}

	public void setArrivalDateAndHour(Date arrivalDateAndHour) {
		this.arrivalDateAndHour = arrivalDateAndHour;
	}

	public int getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(int flightNumber) {
		this.flightNumber = flightNumber;
	}
	
	
	
	
}
	