package main.resources.data;

import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import main.resources.entities.Flight;
import main.resources.hibernate.HibernateUtil;

public class FlightDAO {

	public int addFlight(Flight flight) {
		Session session = HibernateUtil.openSession();
		Transaction transaction = null;

		int flightNumber = -1;

		try {
			transaction = session.beginTransaction();
			flightNumber = (Integer) session.save(flight);
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null)
				transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return flightNumber;
	}

	public List<Flight> getAllFlights() {
		Session session = HibernateUtil.openSession();
		Transaction transaction = null;
		List<Flight> flights = null;
		try {
			transaction = session.beginTransaction();
			flights = session.createQuery("FROM Flight").list();
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			System.out.println("Error getting all flights DAO");
		} finally {
			session.close();
		}
		return flights;
	}

	public Flight getFlight(int id) {
		Session session = HibernateUtil.openSession();
		Transaction transaction = null;
		List<Flight> flights = null;
		try {
			transaction = session.beginTransaction();
			Query query = session.createQuery("FROM Flight WHERE flightNumber=:id");
			query.setParameter("id", id);
			flights = query.list();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			System.out.println("Error getting specific flight DAO");
		} finally {
			session.close();
		}
		if (flights != null && !flights.isEmpty())
			return flights.get(0);
		else
			return null;
	}

	public int updateFlight(int id, Flight flight) {
		Session session = HibernateUtil.openSession();
		Transaction transaction = null;
		int result = 0;

		try {
			transaction = session.beginTransaction();
			Query query = session.createQuery(
					"UPDATE Flight SET airplaneType=:airplaneType,departureCity=:departureCity,arrivalCity=:arrivalCity,departureDateAndHour=:departureDateAndHour,arrivalDateAndHour=:arrivalDateAndHour WHERE flightNumber=:id");
			query.setParameter("airplaneType", flight.getAirplaneType());
			query.setParameter("departureCity", flight.getDepartureCity());
			query.setParameter("arrivalCity", flight.getArrivalCity());
			query.setParameter("departureDateAndHour", flight.getDepartureDateAndHour());
			query.setParameter("arrivalDateAndHour", flight.getArrivalDateAndHour());
			query.setParameter("id", id);
			result = query.executeUpdate();
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			System.out.println("Error update Flights DAO");
		} finally {
			session.close();
		}
		return result;
	}

	public int deleteFlight(int id) {
		Session session = HibernateUtil.openSession();
		Transaction transaction = null;
		int result = 0;
		List<Flight> flights = null;
		try {
			transaction = session.beginTransaction();
			Query query = session.createQuery("FROM Flight WHERE flightNumber=:flightNumber");
			query.setParameter("flightNumber", id);
			flights = query.list();
			if (flights != null && !flights.isEmpty()) {
				session.delete(flights.get(0));
				result = flights.get(0).getFlightNumber();

			}
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			System.out.println("Error delete Flights DAO");
		} finally {
			session.close();
		}
		return result;
	}

}
