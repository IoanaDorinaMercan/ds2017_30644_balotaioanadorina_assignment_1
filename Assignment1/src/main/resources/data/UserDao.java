package main.resources.data;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import main.resources.entities.User;
import main.resources.hibernate.HibernateUtil;

public class UserDao {

	public int addUser(String username, String password, boolean isAdmin) {
		Session session = HibernateUtil.openSession();
		Transaction transaction = null;

		User user = new User();
		user.setUsername(username);
		user.setPassword(password);
		user.setIsAdmin(isAdmin);

		int userId = -1;

		try {
			transaction = session.beginTransaction();
			userId = (Integer) session.save(user);
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null)
				transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return userId;
	}

	public User findUser(String username, String password) {
		Session session = HibernateUtil.openSession();
		Transaction transaction = null;
		List<User> users = null;
		try {
			transaction = session.beginTransaction();
			Query query = session.createQuery("FROM User WHERE username=:username AND password=:password");
			query.setParameter("username", username);
			query.setParameter("password", password);
			users = query.list();
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			System.out.println("Error find user DAO");
		} finally {
			session.close();
		}
		if (users.size() > 0)
			return users.get(0);
		else
			return null;
	}
}
